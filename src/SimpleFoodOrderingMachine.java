import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class SimpleFoodOrderingMachine {
    private JPanel root;
    private JButton ramenButton;
    private JButton udonButton;
    private JButton tempuraButton;
    private JTextPane receivedInfo;
    private JButton gyozaButton;
    private JButton yakisobaButton;
    private JButton karaageButton;
    private JTextPane reservedInfo;
    private JButton checkoutButton;
    private JLabel totalPrice;
    private ArrayList<String> orderedList = new ArrayList<String>();

    public void order(String food) {

        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if (confirmation == 0) {
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you for ordering " + food + "! It will be served as soon as possible"
            );
            orderedList.add(food);
            String setText = "";
            int price = orderedList.size() * 100;
            for(String item : orderedList) {
                setText += item;
                setText += "\n";
            }
            reservedInfo.setText(setText);

            totalPrice.setText(String.valueOf(price) + "円");
        }
    }

    public void checkout() {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like checkout?",
                "Checkout Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if (confirmation == 0) {
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you. The total price is " + String.valueOf(orderedList.size() * 100) + " yen."
            );

            reservedInfo.setText("");
            totalPrice.setText("0円");
        }
    }

    public SimpleFoodOrderingMachine() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura");
            }
        });

        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });

        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }
        });

        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage");
            }
        });

        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba");
            }
        });

        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza");
            }
        });

        checkoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) { checkout(); }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("SimpleFoodOrderingMachine");
        frame.setContentPane(new SimpleFoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
